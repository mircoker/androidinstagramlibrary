package us.stubs.instalib;

import android.os.Handler;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by Ruben on 23.09.2015.
 */
public abstract class SocialNetwork {
    protected Handler handler;
    protected BlockingQueue<Runnable> threadPool;
    protected ThreadPoolExecutor singleThreadPoolExecutor;

}
