package us.stubs.instalib;

/**
 * Created by Ruben on 13.09.2015.
 */


/**
 * This exception caused when password or login are wrong, or other parameters wrong, or server
 * reject ur requests by else reasons (for example to many requests at one time)
 */
public class AccessDeniedException extends Exception {

    public AccessDeniedException(){
        super();
    }

    public AccessDeniedException(String s){
        super(s);
    }
}
