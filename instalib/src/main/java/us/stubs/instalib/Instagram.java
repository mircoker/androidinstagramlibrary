package us.stubs.instalib;

import android.os.Build;
import android.os.Handler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ruben on 22.09.2015.
 */
public class Instagram extends SocialNetwork {

    private final String TOKEN_VARIANT_ACCESS_TOKEN = "access_token";
    private final String TOKEN_VARIANT_CLIENT_ID = "client_id";

    private static Instagram instagram = new Instagram();
    private PrivateAccessMethods privateAccessMethods;
    private PublicAccessMethods publicAccessMethods;


    private Instagram(){
        handler = new Handler();
        threadPool = new ArrayBlockingQueue<>(100);
        singleThreadPoolExecutor = new ThreadPoolExecutor(1,1,5, TimeUnit.SECONDS,threadPool);

        privateAccessMethods = new PrivateAccessMethods();
        publicAccessMethods = new PublicAccessMethods();
    }

    public static Instagram getInstance(){
        return instagram;
    }







    public PrivateAccessMethods getPrivateAccessMethods(){
        return privateAccessMethods;
    }

    public PublicAccessMethods getPublicAccessMethods(){
        return publicAccessMethods;
    }







    public class PrivateAccessMethods {
        private UsersMethods usersMethods;
        private RelationshipMethods relationshipMethods;
        private MediaMethods mediaMethods;
        private CommentsMethods commentsMethods;
        private LikesMethods likesMethods;
        private TagsMethods tagsMethods;
        private LocationsMethods locationsMethods;


        private PrivateAccessMethods(){
            usersMethods = new UsersMethods();
            relationshipMethods = new RelationshipMethods();
            mediaMethods = new MediaMethods();
            commentsMethods = new CommentsMethods();
            likesMethods = new LikesMethods();
            tagsMethods = new TagsMethods();
            locationsMethods = new LocationsMethods();
        }

        public UsersMethods getUsersMethods(){
            return usersMethods;
        }
        public RelationshipMethods getRelationshipMethods(){
            return relationshipMethods;
        }
        public MediaMethods getMediaMethods(){
            return mediaMethods;
        }
        public CommentsMethods getCommentsMethods(){
            return commentsMethods;
        }
        public LikesMethods getLikesMethods(){
            return likesMethods;
        }
        public TagsMethods getTagsMethods(){
            return tagsMethods;
        }
        public LocationsMethods getLocationsMethods(){
            return locationsMethods;
        }


        public class UsersMethods{
            private UsersMethods(){}

            /**
             *
             * Asynchronous variant of {@link #getAccessToken(String, String, String)}
             *
             * Use methods like this carefully.
             * For example if ur activity don't going to be recreated, or u don't care that it going to
             * be recreated(for example if all ur business u do in onCreate(), and u don't interested
             * for old results), or u don't care of memory leaks =).
             * Callback in destroyed activity just don't be called.
             *
             * @param login String, user login
             * @param password String, user password
             * @param clientId String, your application id that defined in instagram API
             *
             * @return
             *
             * @throws RuntimeException Target Activity was destroyed!
             */
            public void getAccessTokenAsync(final String login, final String password, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getAccessToken(login, password, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getFeed(String)}
             *
             * @param accessToken String, access token that can be received from {@link #getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getFeedAsync(final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getFeed(accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }

            /**
             * Asynchronous variant of {@link #getUserInfo(String, String)}
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param accessToken String, access token that can be received from {@link #getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getUserInfoAsync(final String userId, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getUserInfo(userId, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getRecentFeed(String, String)}
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param accessToken String, access token that can be received from {@link #getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getRecentFeedAsync(final String userId, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getRecentFeed(userId, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getLiked(String)}
             *
             * @param accessToken String, access token that can be received from {@link #getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getLikedAsync(final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getLiked(accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #searchUser(String, String)}
             *
             * @param userName String
             * @param accessToken String, access token that can be received from {@link #getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void searchUserAsync(final String userName, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = searchUser(userName, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }


            /**
             * A little hack, that makes possible to build application without webviews.
             *
             * @param login user login
             * @param password user password
             * @param clientId your application id that defined in instagram API
             * @return String, access token
             *
             */
            public String getAccessToken(String login, String password, String clientId) throws AccessDeniedException, IOException, URISyntaxException {
                return getAccessTokenSync(login, password, clientId);
            }
            /**
             *
             * Get basic information about a user.
             *
             * @return String JSON representation (can be revert by new JSONObject(String string))
             *
             *  <p>For example: <pre>{@code
             *   "data": {
             *       "id": "1574083",
             *       "username": "snoopdogg",
             *       "full_name": "Snoop Dogg",
             *       "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1574083_75sq_1295469061.jpg",
             *       "bio": "This is my bio",
             *       "website": "http://snoopdogg.com",
             *       "counts": {
             *           "media": 1320,
             *           "follows": 420,
             *           "followed_by": 3410
             *       }
             *  }
             * }</pre>
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param accessToken String, access token that can be received from {@link #getAccessToken(String, String, String)}
             *
             */
            private synchronized String getUserInfo( String userId, String accessToken ) throws JSONException, AccessDeniedException, IOException {
                return getUserInfoSync(userId, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
            /**
             *
             * Gets the authenticated user's feed.
             *
             * @param accessToken String, access token that can be received from {@link #getAccessToken(String, String, String)}
             *
             * @return String JSON representation (can be revert by new JSONObject(String string))
             *
             *  <p>For example: <pre>
             *
             *{@code
             *   [
             *       "data":
             *           {
             *               "attribution":null,
             *               "tags":[],
             *               "location":null,
             *               "comments":{
             *                   "count":0,
             *                   "data":[]
             *           },
             *               "filter":"Normal",
             *               "created_time":"1442351938",
             *               "link":"https://instagram.com/p/7qoPG_AdiU",
             *               "likes":{
             *                   "count":0,
             *                   "data":[]
             *           },
             *           "images":{
             *               "low_resolution":{
             *                   "url":"https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s320x320/e35/10948867_898029843566200_1938703347_n.jpg",
             *                   "width":320,
             *                   "height":320
             *               },
             *               "thumbnail":{
             *                   "url":"https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s150x150/e35/10948867_898029843566200_1938703347_n.jpg",
             *                   "width":150,
             *                   "height":150
             *               },
             *               "standard_resolution":{
             *                   "url":"https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s320x320/e35/10948867_898029843566200_1938703347_n.jpg",
             *                   "width":320,"height":320
             *               }
             *           },
             *           "users_in_photo":[],
             *           "caption":null,
             *           "type":"image",
             *           "id":"1074848421233416340_1604205117",
             *           "user":{
             *               "username":"mircoker",
             *               "profile_picture":"https://igcdn-photos-h-a.akamaihd.net/hphotos-ak-xfp1/t51.2885-19/10853005_762172850529967_767202471_a.jpg",
             *               "id":"1604205117",
             *               "full_name":"\u0420\u0443\u0431\u0435\u043d \u041c\u043b\u043e\u0434\u0435\u0446\u043a\u0438\u0439"
             *           }
             *       }
             *       {}
             *       {}
             *       {}
             *       ...
             *       "meta":{
             *           "code":200
             *       },
             *       "pagination":{
             *           "next_url":"https://api.instagram.com/v1/users/1604205117/media/recent?max_id=1070293517676436192_1604205117\u0026client_id=c7f9758967714eccb3d0d2c45d6bd6eb",
             *           "next_max_id":"1070293517676436192_1604205117"
             *       }
             *
             *   ]
             *
             * }</pre>
             *
             *
             * @throws JSONException
             * @throws AccessDeniedException
             * @throws IOException
             *
             */
            private synchronized String getFeed( String accessToken ) throws JSONException, AccessDeniedException, IOException {
                return getFeedSync(accessToken);
            }
            /**
             *
             * Get the most recent media published by a user.
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param accessToken String, access token that can be received from {@link #getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             *
             *  <p>For example: <pre>
             *
             *{@code
             *   [
             *       "data":
             *           {
             *               "attribution":null,
             *               "tags":[],
             *               "location":null,
             *               "comments":{
             *                   "count":0,
             *                   "data":[]
             *           },
             *               "filter":"Normal",
             *               "created_time":"1442351938",
             *               "link":"https://instagram.com/p/7qoPG_AdiU",
             *               "likes":{
             *                   "count":0,
             *                   "data":[]
             *           },
             *           "images":{
             *               "low_resolution":{
             *                   "url":"https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s320x320/e35/10948867_898029843566200_1938703347_n.jpg",
             *                   "width":320,
             *                   "height":320
             *               },
             *               "thumbnail":{
             *                   "url":"https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s150x150/e35/10948867_898029843566200_1938703347_n.jpg",
             *                   "width":150,
             *                   "height":150
             *               },
             *               "standard_resolution":{
             *                   "url":"https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s320x320/e35/10948867_898029843566200_1938703347_n.jpg",
             *                   "width":320,"height":320
             *               }
             *           },
             *           "users_in_photo":[],
             *           "caption":null,
             *           "type":"image",
             *           "id":"1074848421233416340_1604205117",
             *           "user":{
             *               "username":"mircoker",
             *               "profile_picture":"https://igcdn-photos-h-a.akamaihd.net/hphotos-ak-xfp1/t51.2885-19/10853005_762172850529967_767202471_a.jpg",
             *               "id":"1604205117",
             *               "full_name":"\u0420\u0443\u0431\u0435\u043d \u041c\u043b\u043e\u0434\u0435\u0446\u043a\u0438\u0439"
             *           }
             *       }
             *       {}
             *       {}
             *       {}
             *       ...
             *       "meta":{
             *           "code":200
             *       },
             *       "pagination":{
             *           "next_url":"https://api.instagram.com/v1/users/1604205117/media/recent?max_id=1070293517676436192_1604205117\u0026client_id=c7f9758967714eccb3d0d2c45d6bd6eb",
             *           "next_max_id":"1070293517676436192_1604205117"
             *       }
             *
             *   ]
             *
             * }</pre>
             *
             *
             * @throws JSONException
             * @throws AccessDeniedException
             * @throws IOException
             *
             */
            public String getRecentFeed(String userId, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getRecentFeedSync(userId, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
            /**
             * Returns content that user was liked.
             *
             * @param accessToken String, access token that can be received from {@link #getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             *
             *  <p>For example: <pre>
             *
             *{@code
             *{
             *    "data": [{
             *        "location": {
             *            "id": "833",
             *            "latitude": 37.77956816727314,
             *            "longitude": -122.41387367248539,
             *            "name": "Civic Center BART"
             *        },
             *        "comments": {
             *            "count": 16,
             *            "data": [ ... ]
             *        },
             *        "caption": null,
             *        "link": "http://instagr.am/p/BXsFz/",
             *        "likes": {
             *            "count": 190,
             *            "data": [{
             *                "username": "shayne",
             *                "full_name": "Shayne Sweeney",
             *                "id": "20",
             *                "profile_picture": "..."
             *            }, {...subset of likers...}]
             *        },
             *        "created_time": "1296748524",
             *        "images": {
             *            "low_resolution": {
             *                "url": "http://distillery.s3.amazonaws.com/media/2011/02/03/efc502667a554329b52d9a6bab35b24a_6.jpg",
             *                "width": 306,
             *                "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distillery.s3.amazonaws.com/media/2011/02/03/efc502667a554329b52d9a6bab35b24a_5.jpg",
             *                "width": 150,
             *                "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distillery.s3.amazonaws.com/media/2011/02/03/efc502667a554329b52d9a6bab35b24a_7.jpg",
             *                "width": 612,
             *                "height": 612
             *            }
             *        },
             *        "type": "image",
             *        "users_in_photo": [],
             *        "filter": "Earlybird",
             *        "tags": [],
             *        "id": "22987123",
             *        "user": {
             *            "username": "kevin",
             *            "full_name": "Kevin S",
             *            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_3_75sq_1295574122.jpg",
             *            "id": "3"
             *        }
             *    },
             *    {
             *        "videos": {
             *            "low_resolution": {
             *                "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
             *                "width": 480,
             *                "height": 480
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
             *                "width": 640,
             *                "height": 640
             *            },
             *        "comments": {
             *            "data": [{
             *                "created_time": "1279332030",
             *                "text": "Love the sign here",
             *                "from": {
             *                    "username": "mikeyk",
             *                    "full_name": "Mikey Krieger",
             *                    "id": "4",
             *                    "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *                },
             *                "id": "8"
             *            },
             *            {
             *                "created_time": "1279341004",
             *                "text": "Chilako taco",
             *                "from": {
             *                    "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "id": "3",
             *                    "profile_picture": "..."
             *                },
             *                "id": "3"
             *            }],
             *            "count": 2
             *        },
             *        "caption": null,
             *        "likes": {
             *            "count": 1,
             *            "data": [{
             *                "username": "mikeyk",
             *                "full_name": "Mikeyk",
             *                "id": "4",
             *                "profile_picture": "..."
             *            }]
             *        },
             *        "link": "http://instagr.am/p/D/",
             *        "created_time": "1279340983",
             *        "images": {
             *            "low_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
             *                "width": 306,
             *                "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
             *                "width": 150,
             *                "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
             *                "width": 612,
             *                "height": 612
             *            }
             *        },
             *        "type": "video",
             *        "users_in_photo": null,
             *        "filter": "Vesper",
             *        "tags": [],
             *        "id": "363839373298",
             *        "user": {
             *            "username": "kevin",
             *            "full_name": "Kevin S",
             *            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_3_75sq_1295574122.jpg",
             *            "id": "3"
             *        },
             *        "location": null
             *    },
             *    ...]
             *  }
             *
             * }
             *
             * }</pre>
             *
             *
             * @throws JSONException
             * @throws AccessDeniedException
             * @throws IOException
             */
            public String getLiked(String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getLikedSync(accessToken);
            }
            /**
             *
             * Search for a user by name. Returns scope of closest names.
             *
             * @param userName String
             * @param accessToken String, access token that can be received from {@link #getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             *
             *  <p>For example: <pre>
             *
             *{@code
             *
             *{
             *    "data": [{
             *    "username": "jack",
             *            "first_name": "Jack",
             *            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_66_75sq.jpg",
             *            "id": "66",
             *            "last_name": "Dorsey"
             *},
             *    {
             *        "username": "sammyjack",
             *            "first_name": "Sammy",
             *            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_29648_75sq_1294520029.jpg",
             *            "id": "29648",
             *            "last_name": "Jack"
             *    },
             *    {
             *        "username": "jacktiddy",
             *            "first_name": "Jack",
             *            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_13096_75sq_1286441317.jpg",
             *            "id": "13096",
             *            "last_name": "Tiddy"
             *    }]
             *}
             *
             * }</pre>
             *
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String searchUser(String userName, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return searchUserSync(userName, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
        }


        public class RelationshipMethods{
            private RelationshipMethods(){}




            /**
             * Asynchronous variant of {@link #getFollows(String, String)}
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getFollowsAsync(final String userId, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getFollows(userId, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getFollowedBy(String, String)}
             *
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getFollowedByAsync(final String userId, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getFollowedBy(userId, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getRequestedBy(String)}
             *
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getRequestedByAsync(final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getRequestedBy(accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getRelationship(String, String)}
             *
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getRelationshipAsync(final String userId, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getRelationship(userId, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }










            /**
             * Get the list of users this user follows.
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "username": "kevin",
             *            "profile_picture": "http://images.ak.instagram.com/profiles/profile_3_75sq_1325536697.jpg",
             *            "full_name": "Kevin Systrom",
             *            "id": "3"
             *},
             *    {
             *        "username": "instagram",
             *            "profile_picture": "http://images.ak.instagram.com/profiles/profile_25025320_75sq_1340929272.jpg",
             *            "full_name": "Instagram",
             *            "id": "25025320"
             *    }]
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getFollows(String userId, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getFollowsSync(userId, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
            /**
             * Get the list of users this user is followed by.
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "username": "kevin",
             *            "profile_picture": "http://images.ak.instagram.com/profiles/profile_3_75sq_1325536697.jpg",
             *            "full_name": "Kevin Systrom",
             *            "id": "3"
             *},
             *    {
             *        "username": "instagram",
             *            "profile_picture": "http://images.ak.instagram.com/profiles/profile_25025320_75sq_1340929272.jpg",
             *            "full_name": "Instagram",
             *            "id": "25025320"
             *    }]
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getFollowedBy(String userId, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getFollowedBySync(userId, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
            /**
             * List the users who have requested this user's permission to follow.
             *
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "meta": {
             *    "code": 200
             *},
             *    "data": [
             *    {
             *        "username": "mikeyk",
             *            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_4_75sq_1292324747_debug.jpg",
             *            "id": "4"
             *    }
             *    ]
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getRequestedBy(String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getRequestedBySync(accessToken);
            }
            /**
             * Get information about a relationship to another user.
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "meta": {
             *    "code": 200
             *},
             *    "data": {
             *    "outgoing_status": "none",
             *            "incoming_status": "requested_by"
             *}
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getRelationship(String userId, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getRelationshipSync(userId, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
        }

        public class MediaMethods{
            private MediaMethods(){}


            /**
             * Asynchronous variant of {@link #getMediaInfo(String, String)}
             *
             * @param mediaId String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getMediaInfoAsync(final String mediaId, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getMediaInfo(mediaId, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #searchMedia(String, String, String)}
             *
             * @param lat String
             * @param lng String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void searchMediaAsync(final String lat, final String lng, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = searchMedia(lat, lng, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getPopularMedia(String)}
             *
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getPopularMediaAsync(final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getPopularMedia(accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Get information about a media object. The returned type key will allow you to
             * differentiate between image and video media.
             * Note: if you authenticate with an OAuth Token, you will receive the user_has_liked key
             * which quickly tells you whether the current user has liked this media item.
             *
             * @param mediaId String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": {
             *    "type": "image",
             *            "users_in_photo": [{
             *        "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "id": "3",
             *                    "profile_picture": "..."
             *        },
             *        "position": {
             *            "x": 0.315,
             *                    "y": 0.9111
             *        }
             *    }],
             *    "filter": "Walden",
             *            "tags": [],
             *    "comments": {
             *        "data": [{
             *            "created_time": "1279332030",
             *                    "text": "Love the sign here",
             *                    "from": {
             *                "username": "mikeyk",
             *                        "full_name": "Mikey Krieger",
             *                        "id": "4",
             *                        "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *            },
             *            "id": "8"
             *        },
             *        {
             *            "created_time": "1279341004",
             *                "text": "Chilako taco",
             *                "from": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "id": "3",
             *                    "profile_picture": "..."
             *        },
             *            "id": "3"
             *        }],
             *        "count": 2
             *    },
             *    "caption": null,
             *            "likes": {
             *        "count": 1,
             *                "data": [{
             *            "username": "mikeyk",
             *                    "full_name": "Mikeyk",
             *                    "id": "4",
             *                    "profile_picture": "..."
             *        }]
             *    },
             *    "link": "http://instagr.am/p/D/",
             *            "user": {
             *        "username": "kevin",
             *                "full_name": "Kevin S",
             *                "profile_picture": "...",
             *                "id": "3"
             *    },
             *    "created_time": "1279340983",
             *            "images": {
             *        "low_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2010/07/16/4de37e03aa4b4372843a7eb33fa41cad_6.jpg",
             *                    "width": 306,
             *                    "height": 306
             *        },
             *        "thumbnail": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2010/07/16/4de37e03aa4b4372843a7eb33fa41cad_5.jpg",
             *                    "width": 150,
             *                    "height": 150
             *        },
             *        "standard_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2010/07/16/4de37e03aa4b4372843a7eb33fa41cad_7.jpg",
             *                    "width": 612,
             *                    "height": 612
             *        }
             *    },
             *    "id": "3",
             *            "location": null
             *}
             *}
             *
             *
             *Video Example
             *
             *{
             *    "data": {
             *    "type": "video",
             *            "videos": {
             *        "low_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
             *                    "width": 480,
             *                    "height": 480
             *        },
             *        "standard_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
             *                    "width": 640,
             *                    "height": 640
             *        },
             *        "users_in_photo": null,
             *                "filter": "Vesper",
             *                "tags": [],
             *        "comments": {
             *            "data": [{
             *                "created_time": "1279332030",
             *                        "text": "Love the sign here",
             *                        "from": {
             *                    "username": "mikeyk",
             *                            "full_name": "Mikey Krieger",
             *                            "id": "4",
             *                            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *                },
             *                "id": "8"
             *            },
             *            {
             *                "created_time": "1279341004",
             *                    "text": "Chilako taco",
             *                    "from": {
             *                "username": "kevin",
             *                        "full_name": "Kevin S",
             *                        "id": "3",
             *                        "profile_picture": "..."
             *            },
             *                "id": "3"
             *            }],
             *            "count": 2
             *        },
             *        "caption": null,
             *                "likes": {
             *            "count": 1,
             *                    "data": [{
             *                "username": "mikeyk",
             *                        "full_name": "Mikeyk",
             *                        "id": "4",
             *                        "profile_picture": "..."
             *            }]
             *        },
             *        "link": "http://instagr.am/p/D/",
             *                "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "profile_picture": "...",
             *                    "id": "3"
             *        },
             *        "created_time": "1279340983",
             *                "images": {
             *            "low_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
             *                        "width": 306,
             *                        "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
             *                        "width": 150,
             *                        "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
             *                        "width": 612,
             *                        "height": 612
             *            }
             *        },
             *        "id": "3",
             *                "location": null
             *              }
             *     }
             *}
             *
             *}</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getMediaInfo(String mediaId, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getMediaInfoSync(mediaId, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
            /**
             *
             * Search for media in a given area. The default time span is set to 5 days.
             * The time span must not exceed 7 days. Defaults time stamps cover the last 5 days.
             * Can return mix of image and video types.
             *
             * @param lat String
             * @param lng String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "distance": 41.741369194629698,
             *            "type": "image",
             *            "users_in_photo": [],
             *    "filter": "Earlybird",
             *            "tags": [],
             *    "comments": { ... },
             *    "caption": null,
             *            "likes": { ... },
             *    "link": "http://instagr.am/p/BQEEq/",
             *            "user": {
             *        "username": "mahaface",
             *                "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1329896_75sq_1294131373.jpg",
             *                "id": "1329896"
             *    },
             *    "created_time": "1296251679",
             *            "images": {
             *        "low_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/28/0cc4f24f25654b1c8d655835c58b850a_6.jpg",
             *                    "width": 306,
             *                    "height": 306
             *        },
             *        "thumbnail": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/28/0cc4f24f25654b1c8d655835c58b850a_5.jpg",
             *                    "width": 150,
             *                    "height": 150
             *        },
             *        "standard_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/28/0cc4f24f25654b1c8d655835c58b850a_7.jpg",
             *                    "width": 612,
             *                    "height": 612
             *        }
             *    },
             *    "id": "20988202",
             *            "location": null
             *},
             *    {
             *        "distance": 41.741369194629698,
             *            "type": "video",
             *            "videos": {
             *        "low_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
             *                    "width": 480,
             *                    "height": 480
             *        },
             *        "standard_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
             *                    "width": 640,
             *                    "height": 640
             *        },
             *        "users_in_photo": null,
             *                "filter": "Vesper",
             *                "tags": [],
             *        "comments": {
             *            "data": [{
             *                "created_time": "1279332030",
             *                        "text": "Love the sign here",
             *                        "from": {
             *                    "username": "mikeyk",
             *                            "full_name": "Mikey Krieger",
             *                            "id": "4",
             *                            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *                },
             *                "id": "8"
             *            },
             *            {
             *                "created_time": "1279341004",
             *                    "text": "Chilako taco",
             *                    "from": {
             *                "username": "kevin",
             *                        "full_name": "Kevin S",
             *                        "id": "3",
             *                        "profile_picture": "..."
             *            },
             *                "id": "3"
             *            }],
             *            "count": 2
             *        },
             *        "caption": null,
             *                "likes": {
             *            "count": 1,
             *                    "data": [{
             *                "username": "mikeyk",
             *                        "full_name": "Mikeyk",
             *                        "id": "4",
             *                        "profile_picture": "..."
             *            }]
             *        },
             *        "link": "http://instagr.am/p/D/",
             *                "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "profile_picture": "...",
             *                    "id": "3"
             *        },
             *        "created_time": "1279340983",
             *                "images": {
             *            "low_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
             *                        "width": 306,
             *                        "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
             *                        "width": 150,
             *                        "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
             *                        "width": 612,
             *                        "height": 612
             *            }
             *        },
             *        "id": "3",
             *                "location": null
             *    }
             *        ...
             *        ]
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String searchMedia(String lat, String lng, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return searchMediaSync(lat, lng, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
            /**
             *
             * Get a list of what media is most popular at the moment. Can return mix of image and video types.
             *
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "type": "image",
             *            "users_in_photo": [],
             *    "filter": "Gotham",
             *            "tags": [],
             *    "comments": { ... },
             *    "caption": {
             *        "created_time": "1296656006",
             *                "text": "ãã¼ãâ¥ã¢ããªå§ãã¦ä½¿ã£ã¦ã¿ãã(^^)",
             *                "from": {
             *            "username": "cocomiin",
             *                    "full_name": "",
             *                    "type": "user",
             *                    "id": "1127272"
             *        },
             *        "id": "26329105"
             *    },
             *    "likes": {
             *        "count": 35,
             *                "data": [{
             *            "username": "mikeyk",
             *                    "full_name": "Kevin S",
             *                    "id": "4",
             *                    "profile_picture": "..."
             *        }, {...subset of likers...}]
             *    },
             *    "link": "http://instagr.am/p/BV5v_/",
             *            "user": {
             *        "username": "cocomiin",
             *                "full_name": "Cocomiin",
             *                "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1127272_75sq_1296145633.jpg",
             *                "id": "1127272"
             *    },
             *    "created_time": "1296655883",
             *            "images": {
             *        "low_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/01/34d027f155204a1f98dde38649a752ad_6.jpg",
             *                    "width": 306,
             *                    "height": 306
             *        },
             *        "thumbnail": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/01/34d027f155204a1f98dde38649a752ad_5.jpg",
             *                    "width": 150,
             *                    "height": 150
             *        },
             *        "standard_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/01/34d027f155204a1f98dde38649a752ad_7.jpg",
             *                    "width": 612,
             *                    "height": 612
             *        }
             *    },
             *    "id": "22518783",
             *            "location": null
             *},
             *    {
             *        "type": "video",
             *            "videos": {
             *        "low_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
             *                    "width": 480,
             *                    "height": 480
             *        },
             *        "standard_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
             *                    "width": 640,
             *                    "height": 640
             *        },
             *        "users_in_photo": null,
             *                "filter": "Vesper",
             *                "tags": [],
             *        "comments": {
             *            "data": [{
             *                "created_time": "1279332030",
             *                        "text": "Love the sign here",
             *                        "from": {
             *                    "username": "mikeyk",
             *                            "full_name": "Mikey Krieger",
             *                            "id": "4",
             *                            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *                },
             *                "id": "8"
             *            },
             *            {
             *                "created_time": "1279341004",
             *                    "text": "Chilako taco",
             *                    "from": {
             *                "username": "kevin",
             *                        "full_name": "Kevin S",
             *                        "id": "3",
             *                        "profile_picture": "..."
             *            },
             *                "id": "3"
             *            }],
             *            "count": 2
             *        },
             *        "caption": null,
             *                "likes": {
             *            "count": 1,
             *                    "data": [{
             *                "username": "mikeyk",
             *                        "full_name": "Mikeyk",
             *                        "id": "4",
             *                        "profile_picture": "..."
             *            }]
             *        },
             *        "link": "http://instagr.am/p/D/",
             *                "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "profile_picture": "...",
             *                    "id": "3"
             *        },
             *        "created_time": "1279340983",
             *                "images": {
             *            "low_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
             *                        "width": 306,
             *                        "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
             *                        "width": 150,
             *                        "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
             *                        "width": 612,
             *                        "height": 612
             *            }
             *        },
             *        "id": "3",
             *                "location": null
             *    },
             *        ...]
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getPopularMedia(String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getPopularMediaSync(accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
        }

        public class CommentsMethods {
            private CommentsMethods() {}


            /**
             * Asynchronous variant of {@link #getComments(String,String)}
             *
             * @param mediaId String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getCommentsAsync(final String mediaId, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getComments(mediaId, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }





            /**
             * Get a list of recent comments on a media object.
             *
             * @param mediaId String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "meta": {
             *    "code": 200
             *},
             *    "data": [
             *    {
             *        "created_time": "1280780324",
             *            "text": "Really amazing photo!",
             *            "from": {
             *        "username": "snoopdogg",
             *                "profile_picture": "http://images.instagram.com/profiles/profile_16_75sq_1305612434.jpg",
             *                "id": "1574083",
             *                "full_name": "Snoop Dogg"
             *    },
             *        "id": "420"
             *    },
             *    ...
             *    ]
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getComments(String mediaId, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getCommentsSync(mediaId, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
        }

        public class LikesMethods{
            private LikesMethods(){}


            /**
             * Asynchronous variant of {@link #getLikes(String,String)}
             *
             * @param mediaId String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getLikesAsync(final String mediaId, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getLikes(mediaId, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }






            /**
             * Get a list of users who have liked this media.
             *
             * @param mediaId String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "username": "jack",
             *            "first_name": "Jack",
             *            "last_name": "Dorsey",
             *            "type": "user",
             *            "id": "66"
             *},
             *    {
             *        "username": "sammyjack",
             *            "first_name": "Sammy",
             *            "last_name": "Jack",
             *            "type": "user",
             *            "id": "29648"
             *    }]
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getLikes(String mediaId, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getLikesSync(mediaId, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
        }

        public class TagsMethods{
            private TagsMethods(){}


            /**
             * Asynchronous variant of {@link #getTagInfo(String,String)}
             *
             * @param tagName String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getTagInfoAsync(final String tagName, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getTagInfo(tagName, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getRecentTags(String,String)}
             *
             * @param tagName String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getRecentTagsAsync(final String tagName, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getRecentTags(tagName, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }

            /**
             * Asynchronous variant of {@link #searchTags(String,String)}
             *
             * @param tagName String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void searchTagsAsync(final String tagName, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = searchTags(tagName, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }


            /**
             * Get information about a tag object.
             *
             * @param tagName String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": {
             *    "media_count": 472,
             *            "name": "nofilter",
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getTagInfo(String tagName, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getTagInfoSync(tagName, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
            /**
             * Get a list of recently tagged media.
             *
             * @param tagName String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "type": "image",
             *            "users_in_photo": [],
             *    "filter": "Earlybird",
             *            "tags": ["snow"],
             *    "comments": {
             *        "data": [{
             *            "created_time": "1296703540",
             *                    "text": "Snow",
             *                    "from": {
             *                "username": "emohatch",
             *                        "username": "Dave",
             *                        "id": "1242695"
             *            },
             *            "id": "26589964"
             *        },
             *        {
             *            "created_time": "1296707889",
             *                "text": "#snow",
             *                "from": {
             *            "username": "emohatch",
             *                    "username": "Emo Hatch",
             *                    "id": "1242695"
             *        },
             *            "id": "26609649"
             *        }],
             *        "count": 3
             *    }
             *    "caption": {
             *        "created_time": "1296703540",
             *                "text": "#Snow",
             *                "from": {
             *            "username": "emohatch",
             *                    "id": "1242695"
             *        },
             *        "id": "26589964"
             *    },
             *    "likes": {
             *        "count": 1,
             *                "data": [{
             *            "username": "mikeyk",
             *                    "full_name": "Mike Krieger",
             *                    "id": "4",
             *                    "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *        }]
             *    },
             *    "link": "http://instagr.am/p/BWl6P/",
             *            "user": {
             *        "username": "emohatch",
             *                "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg",
             *                "id": "1242695",
             *                "full_name": "Dave"
             *    },
             *    "created_time": "1296703536",
             *            "images": {
             *        "low_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/02/f9443f3443484c40b4792fa7c76214d5_6.jpg",
             *                    "width": 306,
             *                    "height": 306
             *        },
             *        "thumbnail": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/02/f9443f3443484c40b4792fa7c76214d5_5.jpg",
             *                    "width": 150,
             *                    "height": 150
             *        },
             *        "standard_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/02/f9443f3443484c40b4792fa7c76214d5_7.jpg",
             *                    "width": 612,
             *                    "height": 612
             *        }
             *    },
             *    "id": "22699663",
             *            "location": null
             *},
             *    {
             *        "type": "video",
             *            "videos": {
             *        "low_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
             *                    "width": 480,
             *                    "height": 480
             *        },
             *        "standard_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
             *                    "width": 640,
             *                    "height": 640
             *        },
             *        "users_in_photo": null,
             *                "filter": "Vesper",
             *                "tags": ["snow"],
             *        "comments": {
             *            "data": [{
             *                "created_time": "1279332030",
             *                        "text": "Love the sign here",
             *                        "from": {
             *                    "username": "mikeyk",
             *                            "full_name": "Mikey Krieger",
             *                            "id": "4",
             *                            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *                },
             *                "id": "8"
             *            },
             *            {
             *                "created_time": "1279341004",
             *                    "text": "Chilako taco",
             *                    "from": {
             *                "username": "kevin",
             *                        "full_name": "Kevin S",
             *                        "id": "3",
             *                        "profile_picture": "..."
             *            },
             *                "id": "3"
             *            }],
             *            "count": 2
             *        },
             *        "caption": null,
             *                "likes": {
             *            "count": 1,
             *                    "data": [{
             *                "username": "mikeyk",
             *                        "full_name": "Mikeyk",
             *                        "id": "4",
             *                        "profile_picture": "..."
             *            }]
             *        },
             *        "link": "http://instagr.am/p/D/",
             *                "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "profile_picture": "...",
             *                    "id": "3"
             *        },
             *        "created_time": "1279340983",
             *                "images": {
             *            "low_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
             *                        "width": 306,
             *                        "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
             *                        "width": 150,
             *                        "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
             *                        "width": 612,
             *                        "height": 612
             *            }
             *        },
             *        "id": "3",
             *                "location": null
             *    },
             *        ...]
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getRecentTags(String tagName, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getRecentTagsSync(tagName, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
            /**
             * Search for tags by name.
             *
             * @param tagName String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *"data": [
             *{
             *    "media_count": 43590,
             *        "name": "snowy"
             *},
             *{
             *    "media_count": 3264,
             *        "name": "snowyday"
             *},
             *{
             *    "media_count": 1880,
             *        "name": "snowymountains"
             *},
             *{
             *    "media_count": 1164,
             *        "name": "snowydays"
             *},
             *{
             *    "media_count": 776,
             *        "name": "snowyowl"
             *},
             *{
             *    "media_count": 680,
             *        "name": "snowynight"
             *},
             *{
             *    "media_count": 568,
             *        "name": "snowylebanon"
             *},
             *{
             *    "media_count": 522,
             *        "name": "snowymountain"
             *},
             *{
             *    "media_count": 490,
             *        "name": "snowytrees"
             *},
             *{
             *    "media_count": 260,
             *        "name": "snowynights"
             *},
             *{
             *    "media_count": 253,
             *        "name": "snowyegret"
             *},
             *{
             *    "media_count": 223,
             *        "name": "snowytree"
             *},
             *{
             *    "media_count": 214,
             *        "name": "snowymorning"
             *},
             *{
             *    "media_count": 212,
             *        "name": "snowyweather"
             *},
             *{
             *    "media_count": 161,
             *        "name": "snowyoursupport"
             *},
             *{
             *    "media_count": 148,
             *        "name": "snowyrange"
             *},
             *{
             *    "media_count": 136,
             *        "name": "snowynui3z"
             *},
             *{
             *    "media_count": 128,
             *        "name": "snowypeaks"
             *},
             *{
             *    "media_count": 124,
             *        "name": "snowy_dog"
             *},
             *{
             *    "media_count": 120,
             *        "name": "snowyroad"
             *},
             *{
             *    "media_count": 108,
             *        "name": "snowyoghurt"
             *},
             *{
             *    "media_count": 107,
             *        "name": "snowyriver"
             *}
             *],
             *        "meta": {
             *    "code": 200
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String searchTags(String tagName, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return searchTagsSync(tagName, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
        }

        public class LocationsMethods{
            private LocationsMethods(){}




            /**
             * Asynchronous variant of {@link #getLocationInfo(String,String)}
             *
             * @param locationId String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getLocationInfoAsync(final String locationId, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getLocationInfo(locationId, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getRecentMediaForLocation(String,String)}
             *
             * @param locationId String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void getRecentMediaForLocationAsync(final String locationId, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getRecentMediaForLocation(locationId, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #searchLocation(String,String,String)}
             *
             * @param lat String
             * @param lng String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @param callback SocialNetworkCallback
             */
            public void searchLocationAsync(final String lat, final String lng, final String accessToken, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = searchLocation(lat, lng, accessToken);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }







            /**
             * Get information about a location.
             *
             * @param locationId String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": {
             *    "id": "1",
             *            "name": "Dogpatch Labs"
             *    "latitude": 37.782,
             *            "longitude": -122.387,
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getLocationInfo(String locationId, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getLocationInfoSync(locationId, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
            /**
             * Get a list of recent media objects from a given location.
             *
             * @param locationId String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "type": "image",
             *            "users_in_photo": [],
             *    "filter": "Earlybird",
             *            "tags": ["expobar"],
             *    "comments": {
             *        data: [],
             *        count: 0
             *    },
             *    "caption": {
             *        "created_time": "1296532028",
             *                "text": "@mikeyk pulls a shot on our #Expobar",
             *                "from": {
             *            "username": "josh",
             *                    "full_name": "Josh Riedel",
             *                    "type": "user",
             *                    "id": "33"
             *        },
             *        "id": "25663923"
             *    },
             *    "likes": {
             *        "count": 35,
             *                "data": [{
             *            "username": "mikeyk",
             *                    "full_name": "Mikeyk",
             *                    "id": "4",
             *                    "profile_picture": "..."
             *        }, {...subset of likers...}]
             *    },
             *    "link": "http://instagr.am/p/BUS3X/",
             *            "user": {
             *        "username": "josh",
             *                "profile_picture": "...",
             *                "id": "33"
             *    },
             *    "created_time": "1296531955",
             *            "images": {
             *        "low_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/31/32d364527512437a8a17ba308a7c83bb_6.jpg",
             *                    "width": 306,
             *                    "height": 306
             *        },
             *        "thumbnail": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/31/32d364527512437a8a17ba308a7c83bb_5.jpg",
             *                    "width": 150,
             *                    "height": 150
             *        },
             *        "standard_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/31/32d364527512437a8a17ba308a7c83bb_7.jpg",
             *                    "width": 612,
             *                    "height": 612
             *        }
             *    },
             *    "user_has_liked": false,
             *            "id": "22097367",
             *            "location": {
             *        "latitude": 37.780885099999999,
             *                "id": "514276",
             *                "longitude": -122.3948632,
             *                "name": "Instagram"
             *    }
             *},
             *    {
             *        "type": "video",
             *            "videos": {
             *        "low_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
             *                    "width": 480,
             *                    "height": 480
             *        },
             *        "standard_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
             *                    "width": 640,
             *                    "height": 640
             *        },
             *        "users_in_photo": null,
             *                "filter": "Vesper",
             *                "tags": [],
             *        "comments": {
             *            "data": [{
             *                "created_time": "1279332030",
             *                        "text": "Love the sign here",
             *                        "from": {
             *                    "username": "mikeyk",
             *                            "full_name": "Mikey Krieger",
             *                            "id": "4",
             *                            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *                },
             *                "id": "8"
             *            },
             *            {
             *                "created_time": "1279341004",
             *                    "text": "Chilako taco",
             *                    "from": {
             *                "username": "kevin",
             *                        "full_name": "Kevin S",
             *                        "id": "3",
             *                        "profile_picture": "..."
             *            },
             *                "id": "3"
             *            }],
             *            "count": 2
             *        },
             *        "caption": null,
             *                "likes": {
             *            "count": 1,
             *                    "data": [{
             *                "username": "mikeyk",
             *                        "full_name": "Mikeyk",
             *                        "id": "4",
             *                        "profile_picture": "..."
             *            }]
             *        },
             *        "link": "http://instagr.am/p/D/",
             *                "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "profile_picture": "...",
             *                    "id": "3"
             *        },
             *        "created_time": "1279340983",
             *                "images": {
             *            "low_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
             *                        "width": 306,
             *                        "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
             *                        "width": 150,
             *                        "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
             *                        "width": 612,
             *                        "height": 612
             *            }
             *        },
             *        "id": "3",
             *                "location": {
             *            "latitude": 37.780885099999999,
             *                    "id": "514276",
             *                    "longitude": -122.3948632,
             *                    "name": "Instagram"
             *        }
             *    },
             *        ...]
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getRecentMediaForLocation(String locationId, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return getRecentMediaForLocationSync(locationId, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
            /**
             * Search for a location by geographic coordinate.
             *
             * @param lat String
             * @param lng String
             * @param accessToken String, access token that can be received from {@link us.stubs.instalib.Instagram.PrivateAccessMethods.UsersMethods#getAccessToken(String, String, String)}
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "id": "788029",
             *            "latitude": 48.858844300000001,
             *            "longitude": 2.2943506,
             *            "name": "Eiffel Tower, Paris"
             *},
             *    {
             *        "id": "545331",
             *            "latitude": 48.858334059662262,
             *            "longitude": 2.2943401336669909,
             *            "name": "Restaurant 58 Tour Eiffel"
             *    },
             *    {
             *        "id": "421930",
             *            "latitude": 48.858325999999998,
             *            "longitude": 2.294505,
             *            "name": "American Library in Paris"
             *    }]
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String searchLocation(String lat, String lng, String accessToken) throws AccessDeniedException, IOException, JSONException {
                return searchLocationSync(lat, lng, accessToken, TOKEN_VARIANT_ACCESS_TOKEN);
            }
        }




    }


    public class PublicAccessMethods {
        private UsersMethods usersMethods;
        private RelationshipMethods relationshipMethods;
        private MediaMethods mediaMethods;
        private CommentsMethods commentsMethods;
        private LikesMethods likesMethods;
        private TagsMethods tagsMethods;
        private LocationsMethods locationsMethods;


        private PublicAccessMethods(){
            usersMethods = new UsersMethods();
            relationshipMethods = new RelationshipMethods();
            mediaMethods = new MediaMethods();
            commentsMethods = new CommentsMethods();
            likesMethods = new LikesMethods();
            tagsMethods = new TagsMethods();
            locationsMethods = new LocationsMethods();
        }

        public UsersMethods getUsersMethods(){
            return usersMethods;
        }

        public RelationshipMethods getRelationshipMethods(){
            return relationshipMethods;
        }
        public MediaMethods getMediaMethods(){
            return mediaMethods;
        }
        public CommentsMethods getCommentsMethods(){
            return commentsMethods;
        }
        public LikesMethods getLikesMethods(){
            return likesMethods;
        }
        public TagsMethods getTagsMethods(){
            return tagsMethods;
        }
        public LocationsMethods getLocationsMethods() {
            return locationsMethods;
        }




        public class UsersMethods{
            private UsersMethods(){}





            /**
             * Asynchronous variant of {@link #getRecentFeed(String, String)}
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getRecentFeedAsync(final String userId, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getRecentFeed(userId, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #searchUser(String, String)}
             *
             * @param userName String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void searchUserAsync(final String userName, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = searchUser(userName, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getUserId(String, String)}
             *
             * @param userName String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getUserIdAsync(final String userName, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getUserId(userName, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }





            /**
             *
             * Get the most recent media published by a user.
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             *
             *  <p>For example: <pre>
             *
             *{@code
             *   [
             *       "data":
             *           {
             *               "attribution":null,
             *               "tags":[],
             *               "location":null,
             *               "comments":{
             *                   "count":0,
             *                   "data":[]
             *           },
             *               "filter":"Normal",
             *               "created_time":"1442351938",
             *               "link":"https://instagram.com/p/7qoPG_AdiU",
             *               "likes":{
             *                   "count":0,
             *                   "data":[]
             *           },
             *           "images":{
             *               "low_resolution":{
             *                   "url":"https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s320x320/e35/10948867_898029843566200_1938703347_n.jpg",
             *                   "width":320,
             *                   "height":320
             *               },
             *               "thumbnail":{
             *                   "url":"https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s150x150/e35/10948867_898029843566200_1938703347_n.jpg",
             *                   "width":150,
             *                   "height":150
             *               },
             *               "standard_resolution":{
             *                   "url":"https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s320x320/e35/10948867_898029843566200_1938703347_n.jpg",
             *                   "width":320,"height":320
             *               }
             *           },
             *           "users_in_photo":[],
             *           "caption":null,
             *           "type":"image",
             *           "id":"1074848421233416340_1604205117",
             *           "user":{
             *               "username":"mircoker",
             *               "profile_picture":"https://igcdn-photos-h-a.akamaihd.net/hphotos-ak-xfp1/t51.2885-19/10853005_762172850529967_767202471_a.jpg",
             *               "id":"1604205117",
             *               "full_name":"\u0420\u0443\u0431\u0435\u043d \u041c\u043b\u043e\u0434\u0435\u0446\u043a\u0438\u0439"
             *           }
             *       }
             *       {}
             *       {}
             *       {}
             *       ...
             *       "meta":{
             *           "code":200
             *       },
             *       "pagination":{
             *           "next_url":"https://api.instagram.com/v1/users/1604205117/media/recent?max_id=1070293517676436192_1604205117\u0026client_id=c7f9758967714eccb3d0d2c45d6bd6eb",
             *           "next_max_id":"1070293517676436192_1604205117"
             *       }
             *
             *   ]
             *
             * }</pre>
             *
             *
             * @throws JSONException
             * @throws AccessDeniedException
             * @throws IOException
             *
             */
            public String getRecentFeed(String userId, String clientId) throws AccessDeniedException, IOException, JSONException {
                return getRecentFeedSync(userId, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
            /**
             *
             * Search for a user by name. Returns scope of closest names.
             *
             * @param userName String
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             *
             *  <p>For example: <pre>
             *
             *{@code
             *
             *{
             *    "data": [{
             *    "username": "jack",
             *            "first_name": "Jack",
             *            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_66_75sq.jpg",
             *            "id": "66",
             *            "last_name": "Dorsey"
             *},
             *    {
             *        "username": "sammyjack",
             *            "first_name": "Sammy",
             *            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_29648_75sq_1294520029.jpg",
             *            "id": "29648",
             *            "last_name": "Jack"
             *    },
             *    {
             *        "username": "jacktiddy",
             *            "first_name": "Jack",
             *            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_13096_75sq_1286441317.jpg",
             *            "id": "13096",
             *            "last_name": "Tiddy"
             *    }]
             *}
             *
             * }</pre>
             *
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String searchUser(String userName, String clientId) throws AccessDeniedException, IOException, JSONException {
                return searchUserSync(userName, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
            /**
             * Gets constant user ID, defined by instagram system.
             *
             * Issue with random result was fixed by instagram.
             *
             * @param userName String
             * @param clientId your application id that defined in instagram API
             * @return String, user id that defined in instagram, and throws JSONException if scope has no this name
             *
             */
            public synchronized String getUserId(String userName, String clientId) throws AccessDeniedException, JSONException, IOException {

                JSONObject jsonObject = new JSONObject(searchUser(userName, clientId));
                JSONArray jsonArray = jsonObject.getJSONArray("data");

                if( jsonArray.getJSONObject(0).getString("username").equals(userName) ){
                    return jsonArray.getJSONObject(0).getString("id");
                }

                throw new JSONException("There is no element with this name");

            }
        }


        public class RelationshipMethods{
            private RelationshipMethods(){}



            /**
             * Asynchronous variant of {@link #getFollows(String, String)}
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getFollowsAsync(final String userId, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getFollows(userId, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getFollowedBy(String, String)}
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getFollowedByAsync(final String userId, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getFollowedBy(userId, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getRelationship(String, String)}
             *
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getRelationshipAsync(final String userId, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getRelationship(userId, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }





            /**
             * Get the list of users this user follows.
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "username": "kevin",
             *            "profile_picture": "http://images.ak.instagram.com/profiles/profile_3_75sq_1325536697.jpg",
             *            "full_name": "Kevin Systrom",
             *            "id": "3"
             *},
             *    {
             *        "username": "instagram",
             *            "profile_picture": "http://images.ak.instagram.com/profiles/profile_25025320_75sq_1340929272.jpg",
             *            "full_name": "Instagram",
             *            "id": "25025320"
             *    }]
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getFollows(String userId, String clientId) throws AccessDeniedException, IOException, JSONException {
                return getFollowsSync(userId, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
            /**
             * Get the list of users this user is followed by.
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "username": "kevin",
             *            "profile_picture": "http://images.ak.instagram.com/profiles/profile_3_75sq_1325536697.jpg",
             *            "full_name": "Kevin Systrom",
             *            "id": "3"
             *},
             *    {
             *        "username": "instagram",
             *            "profile_picture": "http://images.ak.instagram.com/profiles/profile_25025320_75sq_1340929272.jpg",
             *            "full_name": "Instagram",
             *            "id": "25025320"
             *    }]
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getFollowedBy(String userId, String clientId) throws AccessDeniedException, IOException, JSONException {
                return getFollowedBySync(userId, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
            /**
             * Get information about a relationship to another user.
             *
             * @param userId String, user id that defined in instagram, can be received from {@link us.stubs.instalib.Instagram.PublicAccessMethods.UsersMethods#getUserId(String, String)}
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "meta": {
             *    "code": 200
             *},
             *    "data": {
             *    "outgoing_status": "none",
             *            "incoming_status": "requested_by"
             *}
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getRelationship(String userId, String clientId) throws AccessDeniedException, IOException, JSONException {
                return getRelationshipSync(userId, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
        }

        public class MediaMethods{
            private MediaMethods(){}





            /**
             * Asynchronous variant of {@link #getMediaInfo(String, String)}
             *
             * @param mediaId String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getMediaInfoAsync(final String mediaId, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getMediaInfo(mediaId, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #searchMedia(String, String, String)}
             *
             * @param lat String
             * @param lng String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void searchMediaAsync(final String lat, final String lng, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = searchMedia(lat, lng, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getPopularMedia(String)}
             *
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getPopularMediaAsync(final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getPopularMedia(clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }





            /**
             * Get information about a media object. The returned type key will allow you to
             * differentiate between image and video media.
             * Note: if you authenticate with an OAuth Token, you will receive the user_has_liked key
             * which quickly tells you whether the current user has liked this media item.
             *
             * @param mediaId String
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": {
             *    "type": "image",
             *            "users_in_photo": [{
             *        "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "id": "3",
             *                    "profile_picture": "..."
             *        },
             *        "position": {
             *            "x": 0.315,
             *                    "y": 0.9111
             *        }
             *    }],
             *    "filter": "Walden",
             *            "tags": [],
             *    "comments": {
             *        "data": [{
             *            "created_time": "1279332030",
             *                    "text": "Love the sign here",
             *                    "from": {
             *                "username": "mikeyk",
             *                        "full_name": "Mikey Krieger",
             *                        "id": "4",
             *                        "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *            },
             *            "id": "8"
             *        },
             *        {
             *            "created_time": "1279341004",
             *                "text": "Chilako taco",
             *                "from": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "id": "3",
             *                    "profile_picture": "..."
             *        },
             *            "id": "3"
             *        }],
             *        "count": 2
             *    },
             *    "caption": null,
             *            "likes": {
             *        "count": 1,
             *                "data": [{
             *            "username": "mikeyk",
             *                    "full_name": "Mikeyk",
             *                    "id": "4",
             *                    "profile_picture": "..."
             *        }]
             *    },
             *    "link": "http://instagr.am/p/D/",
             *            "user": {
             *        "username": "kevin",
             *                "full_name": "Kevin S",
             *                "profile_picture": "...",
             *                "id": "3"
             *    },
             *    "created_time": "1279340983",
             *            "images": {
             *        "low_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2010/07/16/4de37e03aa4b4372843a7eb33fa41cad_6.jpg",
             *                    "width": 306,
             *                    "height": 306
             *        },
             *        "thumbnail": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2010/07/16/4de37e03aa4b4372843a7eb33fa41cad_5.jpg",
             *                    "width": 150,
             *                    "height": 150
             *        },
             *        "standard_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2010/07/16/4de37e03aa4b4372843a7eb33fa41cad_7.jpg",
             *                    "width": 612,
             *                    "height": 612
             *        }
             *    },
             *    "id": "3",
             *            "location": null
             *}
             *}
             *
             *
             *Video Example
             *
             *{
             *    "data": {
             *    "type": "video",
             *            "videos": {
             *        "low_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
             *                    "width": 480,
             *                    "height": 480
             *        },
             *        "standard_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
             *                    "width": 640,
             *                    "height": 640
             *        },
             *        "users_in_photo": null,
             *                "filter": "Vesper",
             *                "tags": [],
             *        "comments": {
             *            "data": [{
             *                "created_time": "1279332030",
             *                        "text": "Love the sign here",
             *                        "from": {
             *                    "username": "mikeyk",
             *                            "full_name": "Mikey Krieger",
             *                            "id": "4",
             *                            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *                },
             *                "id": "8"
             *            },
             *            {
             *                "created_time": "1279341004",
             *                    "text": "Chilako taco",
             *                    "from": {
             *                "username": "kevin",
             *                        "full_name": "Kevin S",
             *                        "id": "3",
             *                        "profile_picture": "..."
             *            },
             *                "id": "3"
             *            }],
             *            "count": 2
             *        },
             *        "caption": null,
             *                "likes": {
             *            "count": 1,
             *                    "data": [{
             *                "username": "mikeyk",
             *                        "full_name": "Mikeyk",
             *                        "id": "4",
             *                        "profile_picture": "..."
             *            }]
             *        },
             *        "link": "http://instagr.am/p/D/",
             *                "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "profile_picture": "...",
             *                    "id": "3"
             *        },
             *        "created_time": "1279340983",
             *                "images": {
             *            "low_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
             *                        "width": 306,
             *                        "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
             *                        "width": 150,
             *                        "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
             *                        "width": 612,
             *                        "height": 612
             *            }
             *        },
             *        "id": "3",
             *                "location": null
             *              }
             *     }
             *}
             *
             *}</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getMediaInfo(String mediaId, String clientId) throws AccessDeniedException, IOException, JSONException {
                return getMediaInfoSync(mediaId, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
            /**
             *
             * Search for media in a given area. The default time span is set to 5 days.
             * The time span must not exceed 7 days. Defaults time stamps cover the last 5 days.
             * Can return mix of image and video types.
             *
             * @param lat String
             * @param lng String
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "distance": 41.741369194629698,
             *            "type": "image",
             *            "users_in_photo": [],
             *    "filter": "Earlybird",
             *            "tags": [],
             *    "comments": { ... },
             *    "caption": null,
             *            "likes": { ... },
             *    "link": "http://instagr.am/p/BQEEq/",
             *            "user": {
             *        "username": "mahaface",
             *                "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1329896_75sq_1294131373.jpg",
             *                "id": "1329896"
             *    },
             *    "created_time": "1296251679",
             *            "images": {
             *        "low_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/28/0cc4f24f25654b1c8d655835c58b850a_6.jpg",
             *                    "width": 306,
             *                    "height": 306
             *        },
             *        "thumbnail": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/28/0cc4f24f25654b1c8d655835c58b850a_5.jpg",
             *                    "width": 150,
             *                    "height": 150
             *        },
             *        "standard_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/28/0cc4f24f25654b1c8d655835c58b850a_7.jpg",
             *                    "width": 612,
             *                    "height": 612
             *        }
             *    },
             *    "id": "20988202",
             *            "location": null
             *},
             *    {
             *        "distance": 41.741369194629698,
             *            "type": "video",
             *            "videos": {
             *        "low_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
             *                    "width": 480,
             *                    "height": 480
             *        },
             *        "standard_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
             *                    "width": 640,
             *                    "height": 640
             *        },
             *        "users_in_photo": null,
             *                "filter": "Vesper",
             *                "tags": [],
             *        "comments": {
             *            "data": [{
             *                "created_time": "1279332030",
             *                        "text": "Love the sign here",
             *                        "from": {
             *                    "username": "mikeyk",
             *                            "full_name": "Mikey Krieger",
             *                            "id": "4",
             *                            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *                },
             *                "id": "8"
             *            },
             *            {
             *                "created_time": "1279341004",
             *                    "text": "Chilako taco",
             *                    "from": {
             *                "username": "kevin",
             *                        "full_name": "Kevin S",
             *                        "id": "3",
             *                        "profile_picture": "..."
             *            },
             *                "id": "3"
             *            }],
             *            "count": 2
             *        },
             *        "caption": null,
             *                "likes": {
             *            "count": 1,
             *                    "data": [{
             *                "username": "mikeyk",
             *                        "full_name": "Mikeyk",
             *                        "id": "4",
             *                        "profile_picture": "..."
             *            }]
             *        },
             *        "link": "http://instagr.am/p/D/",
             *                "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "profile_picture": "...",
             *                    "id": "3"
             *        },
             *        "created_time": "1279340983",
             *                "images": {
             *            "low_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
             *                        "width": 306,
             *                        "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
             *                        "width": 150,
             *                        "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
             *                        "width": 612,
             *                        "height": 612
             *            }
             *        },
             *        "id": "3",
             *                "location": null
             *    }
             *        ...
             *        ]
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String searchMedia(String lat, String lng, String clientId) throws AccessDeniedException, IOException, JSONException {
                return searchMediaSync(lat, lng, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
            /**
             *
             * Get a list of what media is most popular at the moment. Can return mix of image and video types.
             *
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "type": "image",
             *            "users_in_photo": [],
             *    "filter": "Gotham",
             *            "tags": [],
             *    "comments": { ... },
             *    "caption": {
             *        "created_time": "1296656006",
             *                "text": "ãã¼ãâ¥ã¢ããªå§ãã¦ä½¿ã£ã¦ã¿ãã(^^)",
             *                "from": {
             *            "username": "cocomiin",
             *                    "full_name": "",
             *                    "type": "user",
             *                    "id": "1127272"
             *        },
             *        "id": "26329105"
             *    },
             *    "likes": {
             *        "count": 35,
             *                "data": [{
             *            "username": "mikeyk",
             *                    "full_name": "Kevin S",
             *                    "id": "4",
             *                    "profile_picture": "..."
             *        }, {...subset of likers...}]
             *    },
             *    "link": "http://instagr.am/p/BV5v_/",
             *            "user": {
             *        "username": "cocomiin",
             *                "full_name": "Cocomiin",
             *                "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1127272_75sq_1296145633.jpg",
             *                "id": "1127272"
             *    },
             *    "created_time": "1296655883",
             *            "images": {
             *        "low_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/01/34d027f155204a1f98dde38649a752ad_6.jpg",
             *                    "width": 306,
             *                    "height": 306
             *        },
             *        "thumbnail": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/01/34d027f155204a1f98dde38649a752ad_5.jpg",
             *                    "width": 150,
             *                    "height": 150
             *        },
             *        "standard_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/01/34d027f155204a1f98dde38649a752ad_7.jpg",
             *                    "width": 612,
             *                    "height": 612
             *        }
             *    },
             *    "id": "22518783",
             *            "location": null
             *},
             *    {
             *        "type": "video",
             *            "videos": {
             *        "low_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
             *                    "width": 480,
             *                    "height": 480
             *        },
             *        "standard_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
             *                    "width": 640,
             *                    "height": 640
             *        },
             *        "users_in_photo": null,
             *                "filter": "Vesper",
             *                "tags": [],
             *        "comments": {
             *            "data": [{
             *                "created_time": "1279332030",
             *                        "text": "Love the sign here",
             *                        "from": {
             *                    "username": "mikeyk",
             *                            "full_name": "Mikey Krieger",
             *                            "id": "4",
             *                            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *                },
             *                "id": "8"
             *            },
             *            {
             *                "created_time": "1279341004",
             *                    "text": "Chilako taco",
             *                    "from": {
             *                "username": "kevin",
             *                        "full_name": "Kevin S",
             *                        "id": "3",
             *                        "profile_picture": "..."
             *            },
             *                "id": "3"
             *            }],
             *            "count": 2
             *        },
             *        "caption": null,
             *                "likes": {
             *            "count": 1,
             *                    "data": [{
             *                "username": "mikeyk",
             *                        "full_name": "Mikeyk",
             *                        "id": "4",
             *                        "profile_picture": "..."
             *            }]
             *        },
             *        "link": "http://instagr.am/p/D/",
             *                "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "profile_picture": "...",
             *                    "id": "3"
             *        },
             *        "created_time": "1279340983",
             *                "images": {
             *            "low_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
             *                        "width": 306,
             *                        "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
             *                        "width": 150,
             *                        "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
             *                        "width": 612,
             *                        "height": 612
             *            }
             *        },
             *        "id": "3",
             *                "location": null
             *    },
             *        ...]
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getPopularMedia(String clientId) throws AccessDeniedException, IOException, JSONException {
                return getPopularMediaSync(clientId, TOKEN_VARIANT_CLIENT_ID);
            }
        }

        public class CommentsMethods {
            private CommentsMethods() {}





            /**
             * Asynchronous variant of {@link #getComments(String,String)}
             *
             * @param mediaId String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getCommentsAsync(final String mediaId, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getComments(mediaId, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }





            /**
             * Get a list of recent comments on a media object.
             *
             * @param mediaId String
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "meta": {
             *    "code": 200
             *},
             *    "data": [
             *    {
             *        "created_time": "1280780324",
             *            "text": "Really amazing photo!",
             *            "from": {
             *        "username": "snoopdogg",
             *                "profile_picture": "http://images.instagram.com/profiles/profile_16_75sq_1305612434.jpg",
             *                "id": "1574083",
             *                "full_name": "Snoop Dogg"
             *    },
             *        "id": "420"
             *    },
             *    ...
             *    ]
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getComments(String mediaId, String clientId) throws AccessDeniedException, IOException, JSONException {
                return getCommentsSync(mediaId, clientId,TOKEN_VARIANT_CLIENT_ID);
            }
        }

        public class LikesMethods{
            private LikesMethods(){}





            /**
             * Asynchronous variant of {@link #getLikes(String,String)}
             *
             * @param mediaId String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getLikesAsync(final String mediaId, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getLikes(mediaId, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }




            /**
             * Get a list of users who have liked this media.
             *
             * @param mediaId String
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "username": "jack",
             *            "first_name": "Jack",
             *            "last_name": "Dorsey",
             *            "type": "user",
             *            "id": "66"
             *},
             *    {
             *        "username": "sammyjack",
             *            "first_name": "Sammy",
             *            "last_name": "Jack",
             *            "type": "user",
             *            "id": "29648"
             *    }]
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getLikes(String mediaId, String clientId) throws AccessDeniedException, IOException, JSONException {
                return getLikesSync(mediaId, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
        }

        public class TagsMethods{
            private TagsMethods(){}




            /**
             * Asynchronous variant of {@link #getTagInfo(String,String)}
             *
             * @param tagName String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getTagInfoAsync(final String tagName, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getTagInfo(tagName, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getRecentTags(String,String)}
             *
             * @param tagName String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getRecentTagsAsync(final String tagName, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getRecentTags(tagName, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #searchTags(String,String)}
             *
             * @param tagName String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void searchTagsAsync(final String tagName, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = searchTags(tagName, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }





            /**
             * Get information about a tag object.
             *
             * @param tagName String
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": {
             *    "media_count": 472,
             *            "name": "nofilter",
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getTagInfo(String tagName, String clientId) throws AccessDeniedException, IOException, JSONException {
                return getTagInfoSync(tagName, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
            /**
             * Get a list of recently tagged media.
             *
             * @param tagName String
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "type": "image",
             *            "users_in_photo": [],
             *    "filter": "Earlybird",
             *            "tags": ["snow"],
             *    "comments": {
             *        "data": [{
             *            "created_time": "1296703540",
             *                    "text": "Snow",
             *                    "from": {
             *                "username": "emohatch",
             *                        "username": "Dave",
             *                        "id": "1242695"
             *            },
             *            "id": "26589964"
             *        },
             *        {
             *            "created_time": "1296707889",
             *                "text": "#snow",
             *                "from": {
             *            "username": "emohatch",
             *                    "username": "Emo Hatch",
             *                    "id": "1242695"
             *        },
             *            "id": "26609649"
             *        }],
             *        "count": 3
             *    }
             *    "caption": {
             *        "created_time": "1296703540",
             *                "text": "#Snow",
             *                "from": {
             *            "username": "emohatch",
             *                    "id": "1242695"
             *        },
             *        "id": "26589964"
             *    },
             *    "likes": {
             *        "count": 1,
             *                "data": [{
             *            "username": "mikeyk",
             *                    "full_name": "Mike Krieger",
             *                    "id": "4",
             *                    "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *        }]
             *    },
             *    "link": "http://instagr.am/p/BWl6P/",
             *            "user": {
             *        "username": "emohatch",
             *                "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg",
             *                "id": "1242695",
             *                "full_name": "Dave"
             *    },
             *    "created_time": "1296703536",
             *            "images": {
             *        "low_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/02/f9443f3443484c40b4792fa7c76214d5_6.jpg",
             *                    "width": 306,
             *                    "height": 306
             *        },
             *        "thumbnail": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/02/f9443f3443484c40b4792fa7c76214d5_5.jpg",
             *                    "width": 150,
             *                    "height": 150
             *        },
             *        "standard_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/02/02/f9443f3443484c40b4792fa7c76214d5_7.jpg",
             *                    "width": 612,
             *                    "height": 612
             *        }
             *    },
             *    "id": "22699663",
             *            "location": null
             *},
             *    {
             *        "type": "video",
             *            "videos": {
             *        "low_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
             *                    "width": 480,
             *                    "height": 480
             *        },
             *        "standard_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
             *                    "width": 640,
             *                    "height": 640
             *        },
             *        "users_in_photo": null,
             *                "filter": "Vesper",
             *                "tags": ["snow"],
             *        "comments": {
             *            "data": [{
             *                "created_time": "1279332030",
             *                        "text": "Love the sign here",
             *                        "from": {
             *                    "username": "mikeyk",
             *                            "full_name": "Mikey Krieger",
             *                            "id": "4",
             *                            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *                },
             *                "id": "8"
             *            },
             *            {
             *                "created_time": "1279341004",
             *                    "text": "Chilako taco",
             *                    "from": {
             *                "username": "kevin",
             *                        "full_name": "Kevin S",
             *                        "id": "3",
             *                        "profile_picture": "..."
             *            },
             *                "id": "3"
             *            }],
             *            "count": 2
             *        },
             *        "caption": null,
             *                "likes": {
             *            "count": 1,
             *                    "data": [{
             *                "username": "mikeyk",
             *                        "full_name": "Mikeyk",
             *                        "id": "4",
             *                        "profile_picture": "..."
             *            }]
             *        },
             *        "link": "http://instagr.am/p/D/",
             *                "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "profile_picture": "...",
             *                    "id": "3"
             *        },
             *        "created_time": "1279340983",
             *                "images": {
             *            "low_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
             *                        "width": 306,
             *                        "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
             *                        "width": 150,
             *                        "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
             *                        "width": 612,
             *                        "height": 612
             *            }
             *        },
             *        "id": "3",
             *                "location": null
             *    },
             *        ...]
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getRecentTags(String tagName, String clientId) throws AccessDeniedException, IOException, JSONException {
                return getRecentTagsSync(tagName, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
            /**
             * Search for tags by name.
             *
             * @param tagName String
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *"data": [
             *{
             *    "media_count": 43590,
             *        "name": "snowy"
             *},
             *{
             *    "media_count": 3264,
             *        "name": "snowyday"
             *},
             *{
             *    "media_count": 1880,
             *        "name": "snowymountains"
             *},
             *{
             *    "media_count": 1164,
             *        "name": "snowydays"
             *},
             *{
             *    "media_count": 776,
             *        "name": "snowyowl"
             *},
             *{
             *    "media_count": 680,
             *        "name": "snowynight"
             *},
             *{
             *    "media_count": 568,
             *        "name": "snowylebanon"
             *},
             *{
             *    "media_count": 522,
             *        "name": "snowymountain"
             *},
             *{
             *    "media_count": 490,
             *        "name": "snowytrees"
             *},
             *{
             *    "media_count": 260,
             *        "name": "snowynights"
             *},
             *{
             *    "media_count": 253,
             *        "name": "snowyegret"
             *},
             *{
             *    "media_count": 223,
             *        "name": "snowytree"
             *},
             *{
             *    "media_count": 214,
             *        "name": "snowymorning"
             *},
             *{
             *    "media_count": 212,
             *        "name": "snowyweather"
             *},
             *{
             *    "media_count": 161,
             *        "name": "snowyoursupport"
             *},
             *{
             *    "media_count": 148,
             *        "name": "snowyrange"
             *},
             *{
             *    "media_count": 136,
             *        "name": "snowynui3z"
             *},
             *{
             *    "media_count": 128,
             *        "name": "snowypeaks"
             *},
             *{
             *    "media_count": 124,
             *        "name": "snowy_dog"
             *},
             *{
             *    "media_count": 120,
             *        "name": "snowyroad"
             *},
             *{
             *    "media_count": 108,
             *        "name": "snowyoghurt"
             *},
             *{
             *    "media_count": 107,
             *        "name": "snowyriver"
             *}
             *],
             *        "meta": {
             *    "code": 200
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String searchTags(String tagName, String clientId) throws AccessDeniedException, IOException, JSONException {
                return searchTagsSync(tagName, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
        }

        public class LocationsMethods{
            private LocationsMethods(){}




            /**
             * Asynchronous variant of {@link #getLocationInfo(String,String)}
             *
             * @param locationId String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getLocationInfoAsync(final String locationId, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getLocationInfo(locationId, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #getRecentMediaForLocation(String,String)}
             *
             * @param locationId String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void getRecentMediaForLocationAsync(final String locationId, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = getRecentMediaForLocation(locationId, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }
            /**
             * Asynchronous variant of {@link #searchLocation(String,String,String)}
             *
             * @param lat String
             * @param lng String
             * @param clientId String, your application id that defined in instagram API
             * @param callback SocialNetworkCallback
             */
            public void searchLocationAsync(final String lat, final String lng, final String clientId, SocialNetworkCallback callback){

                final WeakReference<SocialNetworkCallback> socialNetworkCallbackWeakReference = new WeakReference<>(callback);

                singleThreadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {

                        String result = null;
                        Exception exception = null;
                        try {
                            result = searchLocation(lat, lng, clientId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            exception = e;
                        }

                        handlerPost(result, exception, socialNetworkCallbackWeakReference);

                    }
                });

            }






            /**
             * Get information about a location.
             *
             * @param locationId String
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": {
             *    "id": "1",
             *            "name": "Dogpatch Labs"
             *    "latitude": 37.782,
             *            "longitude": -122.387,
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getLocationInfo(String locationId, String clientId) throws AccessDeniedException, IOException, JSONException {
                return getLocationInfoSync(locationId, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
            /**
             * Get a list of recent media objects from a given location.
             *
             * @param locationId String
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "type": "image",
             *            "users_in_photo": [],
             *    "filter": "Earlybird",
             *            "tags": ["expobar"],
             *    "comments": {
             *        data: [],
             *        count: 0
             *    },
             *    "caption": {
             *        "created_time": "1296532028",
             *                "text": "@mikeyk pulls a shot on our #Expobar",
             *                "from": {
             *            "username": "josh",
             *                    "full_name": "Josh Riedel",
             *                    "type": "user",
             *                    "id": "33"
             *        },
             *        "id": "25663923"
             *    },
             *    "likes": {
             *        "count": 35,
             *                "data": [{
             *            "username": "mikeyk",
             *                    "full_name": "Mikeyk",
             *                    "id": "4",
             *                    "profile_picture": "..."
             *        }, {...subset of likers...}]
             *    },
             *    "link": "http://instagr.am/p/BUS3X/",
             *            "user": {
             *        "username": "josh",
             *                "profile_picture": "...",
             *                "id": "33"
             *    },
             *    "created_time": "1296531955",
             *            "images": {
             *        "low_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/31/32d364527512437a8a17ba308a7c83bb_6.jpg",
             *                    "width": 306,
             *                    "height": 306
             *        },
             *        "thumbnail": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/31/32d364527512437a8a17ba308a7c83bb_5.jpg",
             *                    "width": 150,
             *                    "height": 150
             *        },
             *        "standard_resolution": {
             *            "url": "http://distillery.s3.amazonaws.com/media/2011/01/31/32d364527512437a8a17ba308a7c83bb_7.jpg",
             *                    "width": 612,
             *                    "height": 612
             *        }
             *    },
             *    "user_has_liked": false,
             *            "id": "22097367",
             *            "location": {
             *        "latitude": 37.780885099999999,
             *                "id": "514276",
             *                "longitude": -122.3948632,
             *                "name": "Instagram"
             *    }
             *},
             *    {
             *        "type": "video",
             *            "videos": {
             *        "low_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_102.mp4",
             *                    "width": 480,
             *                    "height": 480
             *        },
             *        "standard_resolution": {
             *            "url": "http://distilleryvesper9-13.ak.instagram.com/090d06dad9cd11e2aa0912313817975d_101.mp4",
             *                    "width": 640,
             *                    "height": 640
             *        },
             *        "users_in_photo": null,
             *                "filter": "Vesper",
             *                "tags": [],
             *        "comments": {
             *            "data": [{
             *                "created_time": "1279332030",
             *                        "text": "Love the sign here",
             *                        "from": {
             *                    "username": "mikeyk",
             *                            "full_name": "Mikey Krieger",
             *                            "id": "4",
             *                            "profile_picture": "http://distillery.s3.amazonaws.com/profiles/profile_1242695_75sq_1293915800.jpg"
             *                },
             *                "id": "8"
             *            },
             *            {
             *                "created_time": "1279341004",
             *                    "text": "Chilako taco",
             *                    "from": {
             *                "username": "kevin",
             *                        "full_name": "Kevin S",
             *                        "id": "3",
             *                        "profile_picture": "..."
             *            },
             *                "id": "3"
             *            }],
             *            "count": 2
             *        },
             *        "caption": null,
             *                "likes": {
             *            "count": 1,
             *                    "data": [{
             *                "username": "mikeyk",
             *                        "full_name": "Mikeyk",
             *                        "id": "4",
             *                        "profile_picture": "..."
             *            }]
             *        },
             *        "link": "http://instagr.am/p/D/",
             *                "user": {
             *            "username": "kevin",
             *                    "full_name": "Kevin S",
             *                    "profile_picture": "...",
             *                    "id": "3"
             *        },
             *        "created_time": "1279340983",
             *                "images": {
             *            "low_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_6.jpg",
             *                        "width": 306,
             *                        "height": 306
             *            },
             *            "thumbnail": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_5.jpg",
             *                        "width": 150,
             *                        "height": 150
             *            },
             *            "standard_resolution": {
             *                "url": "http://distilleryimage2.ak.instagram.com/11f75f1cd9cc11e2a0fd22000aa8039a_7.jpg",
             *                        "width": 612,
             *                        "height": 612
             *            }
             *        },
             *        "id": "3",
             *                "location": {
             *            "latitude": 37.780885099999999,
             *                    "id": "514276",
             *                    "longitude": -122.3948632,
             *                    "name": "Instagram"
             *        }
             *    },
             *        ...]
             *    }
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String getRecentMediaForLocation(String locationId, String clientId) throws AccessDeniedException, IOException, JSONException {
                return getRecentMediaForLocationSync(locationId, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
            /**
             * Search for a location by geographic coordinate.
             *
             * @param lat String
             * @param lng String
             * @param clientId String, your application id that defined in instagram API
             * @return String JSON representation (can be revert by new JSONObject(String string))
             * <p>For example: <pre>
             * {@code
             *{
             *    "data": [{
             *    "id": "788029",
             *            "latitude": 48.858844300000001,
             *            "longitude": 2.2943506,
             *            "name": "Eiffel Tower, Paris"
             *},
             *    {
             *        "id": "545331",
             *            "latitude": 48.858334059662262,
             *            "longitude": 2.2943401336669909,
             *            "name": "Restaurant 58 Tour Eiffel"
             *    },
             *    {
             *        "id": "421930",
             *            "latitude": 48.858325999999998,
             *            "longitude": 2.294505,
             *            "name": "American Library in Paris"
             *    }]
             *}
             * }</pre>
             * @throws AccessDeniedException
             * @throws IOException
             * @throws JSONException
             */
            public String searchLocation(String lat, String lng, String clientId) throws AccessDeniedException, IOException, JSONException {
                return searchLocationSync(lat, lng, clientId, TOKEN_VARIANT_CLIENT_ID);
            }
        }

    }













    private synchronized String getAccessTokenSync(String login, String password, String clientId) throws AccessDeniedException, IOException, URISyntaxException {

        String accessToken = null;

        final CookieManager cookieManager = new CookieManager();


        final HttpURLConnection connectionToMainPage =
                openHttpUrlConnectionTo("https://instagram.com/oauth/authorize/?client_id=" +
                                clientId + "&redirect_uri=http://www.stabs.us&response_type=token",
                        HttpMethod.GET, false, null, null, null);


        Map<String, List<String>> headerFields = connectionToMainPage.getHeaderFields();
        String redirectUrl = headerFields.get("Location").get(0);

        //*********** redirect
        HttpURLConnection connectionRedirectFromMainPage =
                openHttpUrlConnectionTo(redirectUrl, HttpMethod.GET, false, null, null, null);
        //*********** redirect


        if (connectionRedirectFromMainPage.getResponseCode() == HttpStatus.SC_OK) {

            List<String> cookies = headerFields.get("Set-Cookie");

            String loginCsrftoken = "";

            if (cookies != null) {

                int cookiesSize = cookies.size();
                for (int i = 0; i < cookiesSize; i++) {

                    List<HttpCookieApiLvl1Access> parsedCookies = HttpCookieApiLvl1Access.parse(cookies.get(i));
                    int parsedCookiesSize = parsedCookies.size();
                    for (int j = 0; j < parsedCookiesSize; j++) {
                        if (parsedCookies.get(j).getName().equals("csrftoken")) {
                            loginCsrftoken = parsedCookies.get(j).getValue();
                        }

                    }


                }
            }


            //*************** level 2
            HashMap<String, String> mapRequestConnectionLoginProperties = new HashMap<>();
            mapRequestConnectionLoginProperties.put("Referer", redirectUrl);

            final HttpURLConnection connectionLogin =
                    openHttpUrlConnectionTo(redirectUrl, HttpMethod.POST, false, mapRequestConnectionLoginProperties,
                            "csrfmiddlewaretoken=" + loginCsrftoken + "&username=" + login + "&password=" + password, new PreConnect() {
                                @Override
                                public void doThings(HttpURLConnection connection) {
                                    try {
                                        cookieManager.storeCookies(connectionToMainPage);
                                        cookieManager.setCookies(connection);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });


            if (connectionLogin.getResponseCode() != HttpStatus.SC_MOVED_TEMPORARILY) {
                throw new AccessDeniedException();
            }


            //*************** level 3
            headerFields = connectionLogin.getHeaderFields();
            cookies = headerFields.get("Set-Cookie");
            redirectUrl = headerFields.get("Location").get(0);
            String newCsrftokenValue = null;

            if (cookies != null) {

                int cookiesSize = cookies.size();
                for (int i = 0; i < cookiesSize; i++) {

                    List<HttpCookieApiLvl1Access> parsedCookies = HttpCookieApiLvl1Access.parse(cookies.get(i));
                    int parsedCookiesSize = parsedCookies.size();
                    for (int j = 0; j < parsedCookiesSize; j++) {
                        if (parsedCookies.get(j).getName().equals("csrftoken"))
                            newCsrftokenValue = parsedCookies.get(j).getValue();
                    }
                }
            }


            HashMap<String, String> mapRequestAllowAppConnectionProperties = new HashMap<>();
            mapRequestAllowAppConnectionProperties.put("Referer", redirectUrl);

            HttpURLConnection connectionAllowApp =
                    openHttpUrlConnectionTo(redirectUrl, HttpMethod.POST, true, mapRequestAllowAppConnectionProperties,
                            "csrfmiddlewaretoken=" + newCsrftokenValue + "&allow=Authorize", new PreConnect() {
                                @Override
                                public void doThings(HttpURLConnection connection) {
                                    try {
                                        cookieManager.storeCookies(connectionLogin);
                                        cookieManager.setCookies(connection);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });


            headerFields = connectionAllowApp.getHeaderFields();

            URI uri = new URI(headerFields.get("Location").get(0));
            if (uri.toString().contains("access_token")) {

                accessToken = uri.getFragment().substring("access_token".length() + 1);

            }


        }

        return accessToken;
    }








    //Users

    private synchronized String getUserInfoSync( String userId, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/users/"+userId+"/?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    private synchronized String getFeedSync( String accessToken ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/users/self/feed?access_token="+accessToken, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    private synchronized String getRecentFeedSync( String userId, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/users/"+userId+"/media/recent/?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    private synchronized String getLikedSync( String token ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/users/self/media/liked?access_token="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    private synchronized String searchUserSync( String userName, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/users/search?q="+userName+"&"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    //Relationships



    private synchronized String getFollowsSync( String userId, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/users/"+userId+"/follows?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }

    private synchronized String getFollowedBySync( String userId, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/users/"+userId+"/followed-by?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }

    private synchronized String getRequestedBySync( String accessToken ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/users/self/requested-by?access_token="+accessToken, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    private synchronized String getRelationshipSync( String userId, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/users/"+userId+"/relationship?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }



    //Media



    private synchronized String getMediaInfoSync( String mediaId, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/media/"+mediaId+"?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    private synchronized String searchMediaSync( String lat, String lng, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/media/search?lat="+lat+"&lng="+lng+"&"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    private synchronized String getPopularMediaSync(String token, String tokenVariant) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/media/popular?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }



    //Comments



    private synchronized String getCommentsSync( String mediaId, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/media/"+mediaId+"/comments?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }



    //Likes



    private synchronized String getLikesSync( String mediaId, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/media/"+mediaId+"/likes?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }



    //Tags



    private synchronized String getTagInfoSync( String tagName, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/tags/"+tagName+"?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    private synchronized String getRecentTagsSync( String tagName, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/tags/"+tagName+"/media/recent?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    private synchronized String searchTagsSync( String tagName, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/tags/search?q="+tagName+"&"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }



    //Locations



    private synchronized String getLocationInfoSync( String locationId, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/locations/"+locationId+"?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    private synchronized String getRecentMediaForLocationSync( String locationId, String token, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/locations/"+locationId+"/media/recent?"+tokenVariant+"="+token, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }


    private synchronized String searchLocationSync( String lat, String lng, String accessToken, String tokenVariant ) throws JSONException, AccessDeniedException, IOException {
        HttpURLConnection connection =
                openHttpUrlConnectionTo("https://api.instagram.com/v1/locations/search?lat="+lat+"&lng="+lng+"&"+tokenVariant+"="+accessToken, HttpMethod.GET, true, null, null, null);

        return fetchJSONFromConnection(connection).toString();
    }




























    //Work staff methods




    private void handlerPost( final String result, final Exception exception, final WeakReference<SocialNetworkCallback> callbackWeakReference ){

        handler.post(new Runnable() {
            @Override
            public void run() {
                if (callbackWeakReference.get() != null) {
                    callbackWeakReference.get().onDone(result, exception);
                } else {
                    //Activity was destroyed exception
                    try {
                        throw new Exception("Target Activity was destroyed!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private JSONObject fetchJSONFromConnection( HttpURLConnection connection ) throws AccessDeniedException, IOException, JSONException {

        switch (connection.getResponseCode()) {
            case 200:
            case 201:
                InputStream inputStream = connection.getInputStream();
                StringBuilder buffer = new StringBuilder();

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                String resultJson = buffer.toString();
                return new JSONObject(resultJson);

            default: throw new AccessDeniedException("Wrong server response "+connection.getResponseCode());

        }

    }


    private HttpURLConnection openHttpUrlConnectionTo(String urlString, String method, boolean allowRedirect, Map<String,String> requestProperties, String entity, PreConnect preConnect ) throws IOException {

        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setInstanceFollowRedirects(allowRedirect);
        connection.setRequestMethod(method);

        disableConnectionReuseIfNecessary();

        if( requestProperties!=null )
            for( Map.Entry<String, String> keyValue : requestProperties.entrySet() ){
                connection.setRequestProperty(keyValue.getKey(), keyValue.getValue());
            }


        if(preConnect!=null){
            preConnect.doThings(connection);
        }

        if( entity!=null ) {
            DataOutputStream request = new DataOutputStream(connection.getOutputStream());
            request.writeBytes(entity);
            request.flush();
            request.close();

        } else {

            connection.connect();
        }

        return connection;
    }


    private interface PreConnect{
        void doThings( HttpURLConnection connection );
    }



    private void disableConnectionReuseIfNecessary() {
        // HTTP connection reuse which was buggy pre-froyo
        if (Integer.parseInt(Build.VERSION.SDK) < Build.VERSION_CODES.FROYO) {
            System.setProperty("http.keepAlive", "false");
        }
    }


}
