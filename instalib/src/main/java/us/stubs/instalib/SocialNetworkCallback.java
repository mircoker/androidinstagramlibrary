package us.stubs.instalib;

/**
 * Created by Ruben on 23.09.2015.
 */
public interface SocialNetworkCallback {
    void onDone(String result, Exception e);
}
