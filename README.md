# AndroidInstagramLibrary
Android library for instagram communications without webviews.

![bybye webview](/../screenshots/instaLogin.gif?raw=true "bybye webview")

###### just use function:

![hello sweetie native](/../screenshots/getAccessToken.png?raw=true "hello sweetie native")

#### Quick start:

To get access to full API, u must get the access_token:

Async
```
Instagram.getInstance().
  getPrivateAccessMethods().
    getUsersMethods().getAccessTokenAsync("login", "password", "clientId", new SocialNetworkCallback() {
        @Override
        public void onDone(String result, Exception e) {
            //Ur stuff...
        }
    });
```
Sync
```
String accessToken = Instagram.getInstance().
  getPrivateAccessMethods().getUsersMethods().getAccessToken("login","password", "clientId");
```


In the library there are not all methods. Contact me if you need them, and I will add them.
My email: mircoker@gmail.com
