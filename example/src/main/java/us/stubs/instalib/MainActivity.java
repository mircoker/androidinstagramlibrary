package us.stubs.instalib;

import android.app.Activity;
import android.os.Handler;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    private static MySocialNetworkCallback socialNetworkCallback;
    private TextView textView;
    private Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        textView = (TextView)findViewById(R.id.text_view);




        // Use methods like this carefully.
        // For example if ur activity don't going to be recreated, or u don't care that it going to
        // be recreated(for example if all ur business u do in onCreate() like in this example,
        // and u don't interested for old results), or u don't care of memory leaks =).
        // Callback in destroyed activity just don't be called.
        Instagram.getInstance().getPrivateAccessMethods().getUsersMethods().getAccessTokenAsync("login", "password", "clientId", new SocialNetworkCallback() {
            @Override
            public void onDone(String result, Exception e) {

                textView.setText(result != null ? result : e.toString());

            }
        });



        //Use library by this way if u want manage situations when activity was recreated.
        button = (Button)findViewById(R.id.button);
        if(socialNetworkCallback==null){
            socialNetworkCallback = new MySocialNetworkCallback();
        }
        socialNetworkCallback.setActivity(this);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView.setText("");

                Instagram.getInstance().getPrivateAccessMethods().getUsersMethods().getAccessTokenAsync("login", "password", "clientId", socialNetworkCallback);
            }
        });




        //Use synchronous methods like this when u want control threads with ur own architecture.
//        try {
//            Instagram.getInstance().getPrivateAccessMethods().getUsersMethods().getAccessToken("login","password", "clientId");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }



    static class MySocialNetworkCallback implements SocialNetworkCallback {

        private MainActivity activity;

        public void setActivity(MainActivity _activity){
            activity = _activity;
        }

        @Override
        public void onDone(final String result, final Exception e) {


            final Handler handler = new Handler();

            handler.post(new Runnable() {
                @Override
                public void run() {

                    if (!activity.isDestroyed()) {

                        activity.textView.setText(result != null ? result : e.toString());

                        //...other stuff

                    } else {

                        //manage situation when whe still have old activity(wait, refresh, etc.)
                        handler.postDelayed(this, 10);

                    }

                }

            });


        }
    }






}
